/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  Image,
  Dimensions
} from 'react-native';

import Play from './play'

export default class youtube extends Component {
  static title = '<TabBarIOS>';
  static description = 'Tab-based navigation.';
  static displayName = 'TabBarExample';

  state = {
    selectedTab: 'Image',
    notifCount: 0,
    presses: 0,
  };

  _renderContent = (string) => {
    if(string == '1'){
    return (
        <View>
          <Image
            style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height/2-25, marginBottom:5, marginTop:20}}
            source={require('./img/image1.jpg')}
          />
          <Image
            style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height/2}}
            source={require('./img/image2.jpg')}
          />
        </View>
    );
  }else{
    return (
      <Play
          {...this.props}
          navigator={navigator} />
        );
      }
  };

  render() {
    return (
      <TabBarIOS
        unselectedTintColor="yellow"
        tintColor="white"
    //    unselectedItemTintColor="red"
        barTintColor="darkslateblue">
        <TabBarIOS.Item
          title="Image"
          renderAsOriginal
          icon={require('./img/photo-camera.png')}
          selected={this.state.selectedTab === 'Image'}
          onPress={() => {
            this.setState({
              selectedTab: 'Image',
            });
          }}>
          {this._renderContent('1')}
        </TabBarIOS.Item>

        <TabBarIOS.Item
          icon={require('./img/laptop.png')}
    //      selectedIcon={require('./relay.png')}
          renderAsOriginal
          title="Video"
          selected={this.state.selectedTab === 'Video'}
          onPress={() => {
            this.setState({
              selectedTab: 'Video',
              presses: this.state.presses + 1
            });
          }}>
          {this._renderContent('2')}
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
}

var styles = StyleSheet.create({
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'red',
    margin: 50,
  },
});

AppRegistry.registerComponent('youtube', () => youtube);
